Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/

Files: *
Copyright: 2021, Alliance for Open Media
 2019, Alliance for Open Media.
License: BSD-2-clause or BSD-3-clause

Files: Build/linux/* Build/windows/* Config/* Source/* Source/API/* Source/App/* Source/Lib/ASM_AVX512/* Source/Lib/C_DEFAULT/* Source/Lib/Codec/* Source/Lib/Globals/* test/* test/ref/* third_party/*
Copyright: 2016-2024 Alliance for Open Media
 2018-2024 Intel Corporation
 2020 Tencent Corporation
 2019 Netflix, Inc.
License: BSD-3-Clause-Clear

Files: LICENSE-BSD2.md
Copyright: 2019, Alliance for Open Media.
License: BSD-2-clause

Files: LICENSE.md
Copyright: 2021, Alliance for Open Media
License: BSD-3-clause

Files: Source/API/EbSvtAv1Enc.h
Copyright: 2019, 2022, Intel Corporation
License: BSD-3-Clause-Clear

Files: Source/App/app_config.c
 Source/App/app_output_ivf.c
 Source/App/app_output_ivf.h
Copyright: 2019, 2022, Intel Corporation
License: BSD-3-Clause-Clear

Files: Source/Lib/ASM_AVX2/dav1d_x86inc.asm
Copyright: 2005-2022, x264 project
License: ISC

Files: Source/Lib/ASM_AVX2/itx16_avx2.asm
Copyright: 2021, VideoLAN and dav1d authors
 2021, Two Orioles, LLC
 2021, Matthias Dressel
License: BSD-2-clause

Files: Source/Lib/ASM_AVX2/itx_avx2.asm
 Source/Lib/ASM_AVX2/itx_hbd.h
 Source/Lib/ASM_AVX2/itx_lbd.h
 Source/Lib/ASM_AVX2/mc16_avx2.asm
 Source/Lib/ASM_AVX2/mc_avx2.asm
Copyright: 2018-2023, VideoLAN and dav1d authors
 2018-2021, Two Orioles, LLC
License: BSD-2-clause

Files: Source/Lib/ASM_AVX2/mc.h
Copyright: 2019, 2022, Intel Corporation
License: BSD-3-Clause-Clear

Files: Source/Lib/ASM_NEON/dav1d_asm.S
Copyright: 2018, VideoLAN and dav1d authors
 2018, Janne Grunau
License: BSD-2-clause

Files: Source/Lib/ASM_NEON/dav1d_util.S
Copyright: 2018, VideoLAN and dav1d authors
 2015, Martin Storsjo
 2015, Janne Grunau
License: BSD-2-clause

Files: Source/Lib/ASM_NEON/itx.S
Copyright: 2019, Martin Storsjo
 2018, VideoLAN and dav1d authors
License: BSD-2-clause

Files: Source/Lib/ASM_NEON/itx_lbd_neon.h
Copyright: 2018-2023, VideoLAN and dav1d authors
 2018-2021, Two Orioles, LLC
License: BSD-2-clause

Files: Source/Lib/ASM_SSE2/x86inc.asm
Copyright: 2005-2022, x264 project
License: ISC

Files: Source/Lib/Codec/coding_loop.c
 Source/Lib/Codec/enc_dec_process.c
 Source/Lib/Codec/full_loop.c
 Source/Lib/Codec/inter_prediction.c
 Source/Lib/Codec/mode_decision.c
 Source/Lib/Codec/product_coding_loop.c
 Source/Lib/Codec/rc_process.c
Copyright: 2019, Intel Corporation
 2016, Alliance for Open Media.
License: BSD-3-Clause-Clear

Files: Source/Lib/Codec/enc_cdef.c
 Source/Lib/Codec/pass2_strategy.c
Copyright: 2016, 2019, Alliance for Open Media.
License: BSD-3-Clause-Clear

Files: Source/Lib/Codec/initial_rc_process.c
 Source/Lib/Codec/pic_analysis_process.c
Copyright: 2019, 2022, Intel Corporation
License: BSD-3-Clause-Clear

Files: Source/Lib/Codec/pd_process.c
Copyright: 2019, Netflix, Inc.
 2019, Intel Corporation
License: BSD-3-Clause-Clear

Files: Source/Lib/Codec/temporal_filtering.c
Copyright: 2019, Netflix, Inc.
 2019, Intel Corporation
 2016, Alliance for Open Media.
License: BSD-3-Clause-Clear

Files: Source/Lib/Globals/enc_handle.c
 Source/Lib/Globals/enc_settings.c
 Source/Lib/Globals/metadata_handle.h
Copyright: 2019, 2022, Intel Corporation
License: BSD-3-Clause-Clear

Files: debian/*
Copyright: 2021-2024, Collabora, Ltd.
License: BSD-2-clause

Files: third_party/cpuinfo/*
Copyright: 2019, Google LLC
 2017, 2018, Facebook Inc.
 2012-2017, Georgia Institute of Technology
 2010-2012, Marat Dukhan
License: BSD-2-clause

Files: third_party/cpuinfo/deps/clog/*
Copyright: 2018, Marat Dukhan
 2017, Georgia Institute of Technology
 2017, 2018, Facebook Inc.
License: BSD-2-clause

Files: third_party/fastfeat/*
Copyright: 2006, 2008, Edward Rosten
License: BSD-3-clause

Files: third_party/safestringlib/*
Copyright: 2014-2018, Intel Corporation
License: Expat

Files: third_party/safestringlib/ignore_handler_s.c
Copyright: 2018-2022, Intel Corp
 2012, Jonathan Toppins <jtoppins@users.sourceforge.net>
 2012, Cisco Systems
License: Expat

Files: third_party/safestringlib/safe_lib.h
 third_party/safestringlib/safe_lib_errno.h
Copyright: 2021, 2022, Intel Corp
 2012, Jonathan Toppins <jtoppins@users.sourceforge.net>
 2008-2013, Cisco Systems, Inc
 2008, Bo Berry
License: Expat

Files: third_party/safestringlib/safe_str_constraint.c
Copyright: 2012, Jonathan Toppins <jtoppins@users.sourceforge.net>
 2008-2012, Cisco Systems
 2008, Bo Berry
License: Expat

Files: third_party/safestringlib/safe_str_constraint.h
Copyright: 2022, Intel Corp
 2008-2011, Cisco Systems
 2008, Bo Berry
License: Expat

Files: third_party/safestringlib/safe_str_lib.h
Copyright: 2008-2011, 2013, Cisco Systems, Inc
 2008, Bo Berry
License: Expat

Files: third_party/safestringlib/safe_types.h
Copyright: 2012, Jonathan Toppins <jtoppins@users.sourceforge.net>
 2007-2013, Cisco Systems, Inc
 2007, Bo Berry
License: Expat

Files: third_party/safestringlib/safeclib_private.h
Copyright: 2022, Intel Corp
 2012, Jonathan Toppins <jtoppins@users.sourceforge.net>
 2012, 2013, Cisco Systems, Inc
License: Expat

Files: third_party/safestringlib/strcpy_s.c
 third_party/safestringlib/strncpy_s.c
 third_party/safestringlib/strnlen_s.c
Copyright: 2008-2011, Cisco Systems
 2008, Bo Berry
License: Expat

Files: CMakeLists.txt CONTRIBUTING.md Config/Sample.cfg Docs/Contribute.md Source/API/EbSvtAv1Formats.h Source/API/EbSvtAv1Metadata.h Source/Lib/ASM_AVX2/CMakeLists.txt Source/Lib/ASM_AVX2/blend_a64_mask_avx2.c Source/Lib/ASM_AVX2/cdef_avx2.c Source/Lib/ASM_AVX2/comb_avg_sad_intrin_avx2.c Source/Lib/ASM_AVX2/compute_sad_avx2.h Source/Lib/ASM_AVX2/compute_sad_intrin_avx2.c Source/Lib/ASM_AVX2/dwt_avx2.c Source/Lib/ASM_AVX2/highbd_fwd_txfm_avx2.c Source/Lib/ASM_AVX2/highbd_intra_pred_avx2.c Source/Lib/ASM_AVX2/highbd_jnt_convolve_avx2.c Source/Lib/ASM_AVX2/intra_pred_intrin_avx2.c Source/Lib/ASM_AVX2/memory_avx2.h Source/Lib/ASM_AVX2/noise_model_avx2.c Source/Lib/ASM_AVX2/pack_unpack_intrinsic_avx2.c Source/Lib/ASM_AVX2/palette_avx2.c Source/Lib/ASM_AVX2/pic_operators_inline_avx2.h Source/Lib/ASM_AVX2/pic_operators_intrin_avx2.c Source/Lib/ASM_AVX2/pickrst_avx2.c Source/Lib/ASM_AVX2/restoration_pick_avx2.c Source/Lib/ASM_AVX2/transforms_intrin_avx2.c Source/Lib/ASM_AVX2/transpose_avx2.h Source/Lib/ASM_AVX512/synonyms_avx512.h Source/Lib/ASM_AVX512/transpose_avx512.h Source/Lib/ASM_AVX512/transpose_encoder_avx512.h Source/Lib/ASM_AVX512/wiener_convolve_avx512.c Source/Lib/ASM_NEON/cdef_neon.c Source/Lib/ASM_NEON/highbd_fwd_txfm_neon.c Source/Lib/ASM_NEON/highbd_inv_txfm_neon.c Source/Lib/ASM_NEON/pack_unpack_intrin_neon.c Source/Lib/ASM_NEON/transforms_intrin_neon.c Source/Lib/ASM_SSE2/CMakeLists.txt Source/Lib/ASM_SSE2/avc_style_mcp_intrin_sse.c Source/Lib/ASM_SSE2/compute_mean_intrin_sse2.c Source/Lib/ASM_SSE2/compute_mean_sse2.h Source/Lib/ASM_SSE2/dlf_intrin_sse2.c Source/Lib/ASM_SSE2/dlf_sse2.h Source/Lib/ASM_SSE2/highbd_intrapred_sse2.h Source/Lib/ASM_SSE2/intra_pred_av1_intrin_sse2.c Source/Lib/ASM_SSE2/mcp_sse2.h Source/Lib/ASM_SSE2/me_sad_calc_intrin_sse2.c Source/Lib/ASM_SSE2/pack_unpack_intrin_sse2.c Source/Lib/ASM_SSE2/pic_operators_intrin_sse2.c Source/Lib/ASM_SSE2/picture_operators_sse2.asm Source/Lib/ASM_SSE2/picture_operators_sse2.h Source/Lib/ASM_SSE2/x64Macro.asm Source/Lib/ASM_SSE2/x64RegisterUtil.asm Source/Lib/ASM_SSE2/x64inc.asm Source/Lib/ASM_SSE4_1/CMakeLists.txt Source/Lib/ASM_SSE4_1/blend_a64_mask_sse4.c Source/Lib/ASM_SSE4_1/blend_sse4.h Source/Lib/ASM_SSE4_1/cdef_sse4.c Source/Lib/ASM_SSE4_1/compute_sad_intrin_sse4_1.c Source/Lib/ASM_SSE4_1/highbd_fwd_txfm_sse4.c Source/Lib/ASM_SSE4_1/highbd_inv_txfm_sse4.c Source/Lib/ASM_SSE4_1/intrapred_16bit_intrin_sse4_1.c Source/Lib/ASM_SSE4_1/memory_sse4_1.h Source/Lib/ASM_SSE4_1/pic_operators_intrin_sse4_1.c Source/Lib/ASM_SSE4_1/pickrst_sse4.c Source/Lib/ASM_SSSE3/CMakeLists.txt Source/Lib/ASM_SSSE3/highbd_intrapred_intrin_ssse3.c Source/Lib/CMakeLists.txt Source/Lib/C_DEFAULT/encode_txb_ref_c.c Source/Lib/C_DEFAULT/encode_txb_ref_c.h Source/Lib/C_DEFAULT/sad_av1.c Source/Lib/C_DEFAULT/variance.c Source/Lib/Codec/adaptive_mv_pred.c Source/Lib/Codec/aom_dsp_rtcd.c Source/Lib/Codec/aom_dsp_rtcd.h Source/Lib/Codec/av1_common.h Source/Lib/Codec/av1_structs.h Source/Lib/Codec/av1me.c Source/Lib/Codec/av1me.h Source/Lib/Codec/bitstream_unit.c Source/Lib/Codec/blend_a64_mask.c Source/Lib/Codec/cabac_context_model.c Source/Lib/Codec/cabac_context_model.h Source/Lib/Codec/cdef.c Source/Lib/Codec/cdef.h Source/Lib/Codec/cdef_process.c Source/Lib/Codec/coefficients.h Source/Lib/Codec/common_dsp_rtcd.c Source/Lib/Codec/common_dsp_rtcd.h Source/Lib/Codec/common_utils.h Source/Lib/Codec/convolve.c Source/Lib/Codec/convolve.h Source/Lib/Codec/corner_detect.c Source/Lib/Codec/corner_detect.h Source/Lib/Codec/corner_match.c Source/Lib/Codec/corner_match.h Source/Lib/Codec/deblocking_common.c Source/Lib/Codec/deblocking_common.h Source/Lib/Codec/deblocking_filter.c Source/Lib/Codec/deblocking_filter.h Source/Lib/Codec/definitions.h Source/Lib/Codec/dlf_process.c Source/Lib/Codec/dwt.c Source/Lib/Codec/dwt.h Source/Lib/Codec/ec_object.h Source/Lib/Codec/ec_process.c Source/Lib/Codec/ec_process.h Source/Lib/Codec/enc_cdef.h Source/Lib/Codec/enc_inter_prediction.c Source/Lib/Codec/enc_intra_prediction.c Source/Lib/Codec/enc_warped_motion.c Source/Lib/Codec/enc_warped_motion.h Source/Lib/Codec/encoder.h Source/Lib/Codec/entropy_coding.c Source/Lib/Codec/entropy_coding.h Source/Lib/Codec/fft.c Source/Lib/Codec/fft_common.h Source/Lib/Codec/filter.h Source/Lib/Codec/firstpass.c Source/Lib/Codec/firstpass.h Source/Lib/Codec/global_me.c Source/Lib/Codec/global_me.h Source/Lib/Codec/global_me_cost.c Source/Lib/Codec/global_me_cost.h Source/Lib/Codec/global_motion.c Source/Lib/Codec/global_motion.h Source/Lib/Codec/grainSynthesis.c Source/Lib/Codec/grainSynthesis.h Source/Lib/Codec/hash.c Source/Lib/Codec/hash.h Source/Lib/Codec/hash_motion.c Source/Lib/Codec/hash_motion.h Source/Lib/Codec/intra_prediction.c Source/Lib/Codec/intra_prediction.h Source/Lib/Codec/inv_transforms.c Source/Lib/Codec/inv_transforms.h Source/Lib/Codec/k_means_template.h Source/Lib/Codec/mathutils.h Source/Lib/Codec/mcomp.c Source/Lib/Codec/mcomp.h Source/Lib/Codec/md_config_process.c Source/Lib/Codec/md_rate_estimation.c Source/Lib/Codec/motion_estimation.c Source/Lib/Codec/mv.h Source/Lib/Codec/noise_model.c Source/Lib/Codec/noise_model.h Source/Lib/Codec/noise_util.c Source/Lib/Codec/noise_util.h Source/Lib/Codec/palette.c Source/Lib/Codec/pass2_strategy.h Source/Lib/Codec/pcs.c Source/Lib/Codec/pic_buffer_desc.c Source/Lib/Codec/pic_operators.c Source/Lib/Codec/pic_operators.h Source/Lib/Codec/random.h Source/Lib/Codec/ransac.c Source/Lib/Codec/ransac.h Source/Lib/Codec/rc_results.h Source/Lib/Codec/rd_cost.c Source/Lib/Codec/resize.c Source/Lib/Codec/resize.h Source/Lib/Codec/rest_process.c Source/Lib/Codec/restoration.c Source/Lib/Codec/restoration.h Source/Lib/Codec/restoration_pick.c Source/Lib/Codec/restoration_pick.h Source/Lib/Codec/segmentation.c Source/Lib/Codec/segmentation.h Source/Lib/Codec/segmentation_params.c Source/Lib/Codec/segmentation_params.h Source/Lib/Codec/sequence_control_set.h Source/Lib/Codec/src_ops_process.h Source/Lib/Codec/super_res.c Source/Lib/Codec/super_res.h Source/Lib/Codec/svt_psnr.c Source/Lib/Codec/svt_psnr.h Source/Lib/Codec/sys_resource_manager.c Source/Lib/Codec/sys_resource_manager.h Source/Lib/Codec/temporal_filtering.h Source/Lib/Codec/transforms.c Source/Lib/Codec/transforms.h Source/Lib/Codec/warped_motion.c Source/Lib/Codec/warped_motion.h Source/Lib/Globals/metadata_handle.c test/BlockErrorTest.cc test/Convolve8Test.cc test/FilmGrainTest.cc test/FilterIntraPredTest.cc test/ForwardtransformTests.cc test/MotionEstimationTest.cc test/RestorationPickTest.cc test/SatdTest.cc test/SpatialFullDistortionTest.cc test/TemporalFilterTestPlanewise.cc test/acm_random.h test/av1_convolve_scale_test.cc test/corner_match_test.cc test/dwt_test.cc test/e2e_test/CMakeLists.txt test/e2e_test/ConfigEncoder.cc test/e2e_test/ConfigEncoder.h test/e2e_test/ParseUtil.cc test/e2e_test/ParseUtil.h test/e2e_test/SvtAv1E2ETest.cc test/e2e_test/test_data_download_worker.cmake test/e2e_test/test_data_util.cmake test/frame_error_test.cc test/hadamard_test.cc test/highbd_intra_prediction_tests.cc test/highbd_intra_prediction_tests.h test/noise_model_test.cc test/quantize_func_test.cc test/selfguided_filter_test.cc test/ssim_test.cc test/svt_av1_test.cc test/unit_test.h test/unit_test_utility.c test/unit_test_utility.h test/warp_filter_test.cc test/warp_filter_test_util.cc test/warp_filter_test_util.h
Copyright: 2016-2024 Alliance for Open Media
 2018-2024 Intel Corporation
 2020 Tencent Corporation
 2019 Netflix, Inc.
License: BSD-3-Clause-Clear

Files: Source/Lib/Codec/vector.c Source/Lib/Codec/vector.h
Copyright: 2016 Peter Goldsborough
License: Expat

Files: gstreamer-plugin/gstsvtav1enc.c gstreamer-plugin/gstsvtav1enc.h
Copyright: 2019-2022 Intel Corporation
License: LGPL-2.1+

Files: third_party/cpuinfo/src/cpuinfo/common.h
Copyright: 2019 Google LLC
 2017-2018 Facebook Inc.
 2012-2017 Georgia Institute of Technology
 2010-2018 Marat Dukhan
License: BSD-2-clause

Files: third_party/fastfeat/CMakeLists.txt
Copyright: 2006-2008 Edward Rosten (https://github.com/edrosten/fast-C-src)
License: BSD-3-clause
